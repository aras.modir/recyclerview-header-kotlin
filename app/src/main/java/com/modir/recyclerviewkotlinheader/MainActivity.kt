package com.modir.recyclerviewkotlinheader

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.*

class MainActivity : AppCompatActivity() {
    var recyclerView: RecyclerView? = null
    var adapter: MyRecyclerAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerView = findViewById<View>(R.id.my_recycler_view) as RecyclerView
        val linearLayoutManager = LinearLayoutManager(this)
        adapter = MyRecyclerAdapter(list)
        recyclerView!!.layoutManager = linearLayoutManager
        recyclerView!!.adapter = adapter
    }

    private val list: ArrayList<ListItem>
        private get() {
            val arrayList = ArrayList<ListItem>()
            for (j in 0..4) {
                val header = Header()
                header.date = "header$j"
                header.income = 500
                header.expense = -400
                arrayList.add(header)
                for (i in 0..3) {
                    val item = ContentItem()
                    item.title = i.toString() + ""
                    item.subtitle = "A$i"
                    arrayList.add(item)
                }
            }
            return arrayList
        }
}