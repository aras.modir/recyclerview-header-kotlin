package com.modir.recyclerviewkotlinheader

class ContentItem : ListItem() {
    override var title: String? = null
    override var subtitle: String? = null
    var cost: Int? = null
    var date: String? = null
}